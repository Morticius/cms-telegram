<?php

namespace App\Observers;
use App\Models\API\Telegram\Methods\GetMe;
use App\Models\API\Telegram\Request;
use App\Models\API\Telegram\Telegram;
use App\Models\User\Bot;
use App\Models\User\BotLog;

class BotObserver
{
    public function creating(Bot $bot)
    {
        $telegram = new Telegram($bot->token);
        $getMe = new GetMe();

        Request::execute($getMe, $telegram);

        $user = $getMe->getUser();

        $attr = [
            ['bot_api_id', 'id', false],
            ['is_bot', 'is_bot', false],
            ['first_name', 'first_name', false],
            ['last_name', 'last_name', true],
            ['username', 'username', true],
            ['language_code', 'language_code', true],
        ];

        foreach ($attr as $item) {
            if($item[2]) {
                if(property_exists($user, $item[1])) {
                    $bot->{$item[0]} = $user->{$item[1]};
                }
            } else {
                $bot->{$item[0]} = $user->{$item[1]};
            }
        }
    }

    public function created(Bot $bot)
    {
        $log = new BotLog();
        $log->bot_id = $bot->id;
        $log->is_bot = $bot->is_bot;
        $log->first_name = $bot->first_name;
        $log->last_name = $bot->last_name;
        $log->username = $bot->username;
        $log->language_code = $bot->language_code;
        $log->save();
    }
}