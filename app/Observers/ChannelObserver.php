<?php
/**
 * Created by PhpStorm.
 * User: webdev
 * Date: 13.11.18
 * Time: 5:49
 */

namespace App\Observers;


use App\Models\API\Telegram\Methods\GetChat;
use App\Models\API\Telegram\Request;
use App\Models\API\Telegram\Telegram;
use App\Models\User\Channel;
use Exception;

class ChannelObserver
{
    public function creating(Channel $channel)
    {
        $telegram = new Telegram($channel->bot->token);
        $getChat = new GetChat($channel->username);

        Request::execute($getChat, $telegram);

        $chat = $getChat->getChat();

        if($chat->type !== "channel") {
            throw new Exception();
        }

        $channel->channel_api_id = $chat->id;
        $channel->title = $chat->title;
        //TODO need add verification bot is admin
    }

    public function created()
    {
    }
}