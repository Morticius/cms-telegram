<?php

namespace App\Traits;

use Illuminate\Http\Exceptions\HttpResponseException;

trait FormatJsonResponse
{
    /**
     * Decorator for fail request.
     *
     * @param $errors
     * @param int $code
     * @param string $message
     * @return void
     */
    private function initHttpResponseFail($errors, $code = 422, $message = 'Validation errors in your request')
    {
        $data = [
            'message' => $message
        ];

        if (!is_null($errors)) {
            $data['errors'] = $errors;
        }

        throw new HttpResponseException(response()->json($data, $code));

    }

    private function initHttpResponseSuccess($code = 200, $id = null, $route = '', $message = 'The item was created successfully')
    {
        $response = response()->json(['message' => $message], $code);

        if(!is_null($id)) {
            $response->header('Location', route($route, [$id]));
        }

        return $response;
    }
}
