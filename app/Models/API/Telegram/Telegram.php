<?php
/**
 * Created by PhpStorm.
 * User: webdev
 * Date: 09.11.18
 * Time: 22:56
 */

namespace App\Models\API\Telegram;


use Exception;

class Telegram
{
    /**
     * Telegram API key
     *
     * @var string
     */
    protected $apiKey = '';

    /**
     * TelegramOld constructor.
     *
     * @param $apiKey
     * @throws Exception
     */
    public function __construct($apiKey)
    {
        if (empty($apiKey)) {
            throw new Exception('API KEY not defined!');
        }
        preg_match('/(\d+)\:[\w\-]+/', $apiKey, $matches);
        if (!isset($matches[1])) {
            throw new Exception('Invalid API KEY defined!');
        }
        $this->apiKey = $apiKey;
        Request::initialize();
    }

    public function getApiKey()
    {
        return $this->apiKey;
    }
}