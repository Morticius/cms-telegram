<?php
/**
 * Created by PhpStorm.
 * User: webdev
 * Date: 13.11.18
 * Time: 5:31
 */

namespace App\Models\API\Telegram\Methods;

use App\Models\API\Telegram\Entities\Chat;

class GetChat extends Method
{
    public function __construct($chat_id)
    {
        $this->action = 'getChat';
        $this->params['chat_id'] = $chat_id;
    }

    public function getChat()
    {
        return new Chat($this->result);
    }
}