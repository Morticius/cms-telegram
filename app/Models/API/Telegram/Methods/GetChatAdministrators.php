<?php
/**
 * Created by PhpStorm.
 * User: webdev
 * Date: 13.11.18
 * Time: 5:27
 */

namespace App\Models\API\Telegram\Methods;


class GetChatAdministrators extends Method
{
    public function __construct()
    {
        $this->action = 'getChatAdministrators';
    }

//    public function getUser()
//    {
//        return new User($this->result);
//    }
}