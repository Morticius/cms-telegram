<?php
/**
 * Created by PhpStorm.
 * User: webdev
 * Date: 13.11.18
 * Time: 3:55
 */

namespace App\Models\API\Telegram\Methods;


abstract class Method
{
    protected $action = '';
    protected $params = [];
    protected $result = null;

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @return bool
     */
    public function issetParams()
    {
        return count($this->params) !== 0;
    }

    /**
     * @param mixed $params
     */
    public function setParams(array $params): void
    {
        $this->params = $params;
    }

    public function addParam($name, $value): void
    {
        $this->params[$name] = $value;
    }

    /**
     * @param mixed $result
     */
    public function setResult(array $result): void
    {
        $this->result = $result;
    }
}