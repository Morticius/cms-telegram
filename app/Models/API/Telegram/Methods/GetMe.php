<?php
/**
 * Created by PhpStorm.
 * User: webdev
 * Date: 13.11.18
 * Time: 3:59
 */

namespace App\Models\API\Telegram\Methods;


use App\Models\API\Telegram\Entities\User;

class GetMe extends Method
{
    public function __construct()
    {
        $this->action = 'getMe';
    }

    public function getUser()
    {
        return new User($this->result);
    }
}