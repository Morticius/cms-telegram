<?php
/**
 * Created by PhpStorm.
 * User: webdev
 * Date: 09.11.18
 * Time: 22:56
 */

namespace App\Models\API\Telegram;


use App\Models\API\Telegram\Methods\Method;
use Exception;
use GuzzleHttp\Client;

class Request
{
    /**
     * URI of the Telegram API
     *
     * @var string
     */
    private static $apiBaseUri = 'https://api.telegram.org';
    /**
     * Guzzle Client object
     *
     * @var \GuzzleHttp\Client
     */
    private static $client;

    /**
     * Initialize
     *
     *
     * @throws Exception
     */
    public static function initialize()
    {
        self::setClient(new Client(['base_uri' => self::$apiBaseUri]));
    }

    /**
     * Set a custom Guzzle HTTP Client object
     *
     * @param Client $client
     *
     * @throws Exception
     */
    public static function setClient(Client $client)
    {
        if (!($client instanceof Client)) {
            throw new Exception('Invalid GuzzleHttp\Client pointer!');
        }
        self::$client = $client;
    }

    /**
     * Execute HTTP Request
     *
     * @param Method $method
     * @param Telegram $telegram
     */
    public static function execute(Method &$method, Telegram $telegram)
    {
        $uri = '/bot' . $telegram->getApiKey() . '/' . $method->getAction();

        if($method->issetParams()) {
            $uri .= '?' . http_build_query($method->getParams());
        }

        try {
            $response = self::$client->post($uri);
            $result = json_decode((string) $response->getBody());

            if(!$result->ok) {
                throw new Exception();
            }

            $method->setResult((array)$result->result);

        } catch (Exception $e) {

        }
    }
}