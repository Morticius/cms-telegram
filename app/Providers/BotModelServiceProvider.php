<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BotModelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \App\Models\User\Bot::observe(\App\Observers\BotObserver::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
