<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ChannelModelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \App\Models\User\Channel::observe(\App\Observers\ChannelObserver::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
