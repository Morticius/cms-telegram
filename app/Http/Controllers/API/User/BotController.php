<?php

namespace App\Http\Controllers\API\User;

use App\Http\Resources\API\BotCollection;
use App\Models\User\Bot;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\Bot\{BotStoreRequest, BotUpdateRequest};
use App\Traits\FormatJsonResponse;
use App\Http\Resources\API\Bot as BotResource;

class BotController extends Controller
{
    use FormatJsonResponse;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $limit = 7;
        $results = Bot::paginate($limit);
        return response()
            ->json(new BotCollection($results->getCollection()), 200, [
                'Pagination-Count' => $results->total(),
                'Pagination-Page' => $results->currentPage(),
                'Pagination-Limit' => $limit
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BotStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BotStoreRequest $request)
    {
        $bot = (Bot::create($request->validated()))->fresh();
        return $this->initHttpResponseSuccess(201, $bot->id, 'bots.show');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User\Bot $bot
     * @return BotResource|\Illuminate\Http\Response
     */
    public function show(Bot $bot)
    {
        return new BotResource($bot);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BotUpdateRequest $request
     * @param  \App\Models\User\Bot $bot
     * @return BotResource|\Illuminate\Http\Response
     */
    public function update(BotUpdateRequest $request, Bot $bot)
    {
        $bot->update($request->validated());
        return new BotResource($bot);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User\Bot $bot
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Bot $bot)
    {
        $bot->delete();
        return response()->json(null, 204);
    }
}
