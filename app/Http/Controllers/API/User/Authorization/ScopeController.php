<?php

namespace App\Http\Controllers\API\User\Authorization;

use App\Http\Resources\User\Authorization\ScopeCollection;
use App\Models\User\Authorization\Scope;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\Authorization\{ScopeStoreRequest, ScopeUpdateRequest};
use App\Traits\FormatJsonResponse;
use App\Http\Resources\User\Authorization\Scope as ScopeResource;

class ScopeController extends Controller
{
    use FormatJsonResponse;

    /**
     * Display a listing of the resource.
     *
     * @return ScopeCollection|\Illuminate\Http\Response
     */
    public function index()
    {
        $limit = 7;
        $results = Scope::paginate($limit);
        return response()
            ->json(new ScopeCollection($results->getCollection()), 200, [
                'Pagination-Count' => $results->total(),
                'Pagination-Page' => $results->currentPage(),
                'Pagination-Limit' => $limit
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ScopeStoreRequest $request)
    {
        $scope = (Scope::create($request->validated()))->fresh();
        return $this->initHttpResponseSuccess(201, $scope->id, 'scopes.show');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User\Authorization\Scope  $scope
     * @return ScopeResource|\Illuminate\Http\Response
     */
    public function show(Scope $scope)
    {
        return new ScopeResource($scope);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ScopeUpdateRequest $request
     * @param  \App\Models\User\Authorization\Scope $scope
     * @return ScopeResource|\Illuminate\Http\Response
     */
    public function update(ScopeUpdateRequest $request, Scope $scope)
    {
        $scope->update($request->validated());
        return new ScopeResource($scope);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User\Authorization\Scope $scope
     * @return \Illuminate\Http\Response|int
     * @throws \Exception
     */
    public function destroy(Scope $scope)
    {
        $scope->delete();
        return response()->json(null, 204);
    }
}
