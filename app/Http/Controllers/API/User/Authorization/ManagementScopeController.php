<?php

namespace App\Http\Controllers\API\User\Authorization;

use App\Http\Requests\User\Authorization\ScopeManagementRequest;
use App\Models\User\Authorization\Scope;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManagementScopeController extends Controller
{
    public function setUserScopes(ScopeManagementRequest $request)
    {
        $validated = $request->validated();

        $user = User::find($validated['user_id']);
        $scope = Scope::find($validated['scope_ids']);

        $user->scopes()->syncWithoutDetaching($scope);

        return response()->json(null, 202);
    }

    public function unsetUserScopes(ScopeManagementRequest $request)
    {
        $validated = $request->validated();

        $user = User::find($validated['user_id']);
        $scope = Scope::find($validated['scope_ids']);

        $user->scopes()->detach($scope);

        return response()->json(null, 204);
    }
}
