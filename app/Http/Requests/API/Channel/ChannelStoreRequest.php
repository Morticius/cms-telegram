<?php

namespace App\Http\Requests\API\Channel;

use Illuminate\Foundation\Http\FormRequest;

class ChannelStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bot_id' => 'required|integer|exists:bots,id',
            'username' => 'required|string|max:256|unique:channels',
        ];
    }
}
