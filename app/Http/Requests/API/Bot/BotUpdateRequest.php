<?php

namespace App\Http\Requests\API\Bot;

use App\Http\Resources\API\Bot;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use App\Traits\FormatJsonResponse;
use Illuminate\Support\Facades\Auth;

class BotUpdateRequest extends FormRequest
{
    use FormatJsonResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $bot = request()->bot;

        return [
            'token' => 'required|string|max:256|unique:bots,token,'.$bot->id,
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     */
    protected function failedValidation(Validator $validator)
    {
        $this->initHttpResponseFail($validator->errors());
    }
}
