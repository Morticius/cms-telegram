<?php

namespace App\Http\Requests\User\Authorization;

use Illuminate\Foundation\Http\FormRequest;

class ScopeManagementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'scope_ids' => 'required|array',
            'scope_ids.*' => 'exists:scopes,id'
        ];
    }
}
