<?php

namespace App\Http\Requests\User\Authorization;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use App\Traits\FormatJsonResponse;

class ScopeStoreRequest extends FormRequest
{
    use FormatJsonResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'sometimes|required|integer|exists:scopes,id',
            'scope_id' => 'sometimes|required|integer|exists:scopes,id',
            'name' => 'required|string|max:256|unique:scopes',
            'description' => 'sometimes|required|string|min:3|max:1024',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     */
    protected function failedValidation(Validator $validator)
    {
        $this->initHttpResponseFail($validator->errors());
    }
}
