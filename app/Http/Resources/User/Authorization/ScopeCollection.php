<?php

namespace App\Http\Resources\User\Authorization;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\User\Authorization\Scope;

class ScopeCollection extends ResourceCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = 'App\Http\Resources\User\Authorization\Scope';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Support\Collection
     */
    public function toArray($request)
    {
        return $this->collection;
    }
}
