<?php

namespace App\Http\Resources\User\Authorization;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Scope extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->when(!is_null($this->description), $this->description),
            'parent' => $this->scope_id,
            'is_available' => $this->is_available,
            'is_default' => $this->is_default,
            'created_at' => (string)$this->created_at,
            'updated_at' => (string)$this->updated_at,
        ];
    }
}
