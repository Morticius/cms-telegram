<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScopesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scopes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('scope_id')->nullable(true);
            $table->string('name', 64);
            $table->string('description', 1024)->nullable(true);
            $table->boolean('is_available')->default(true);
            $table->boolean('is_default')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('scopes', function (Blueprint $table) {
            $table->unique('name');
            $table->index('scope_id');
        });

        Schema::table('scopes', function (Blueprint $table) {
            $table->foreign('scope_id')->references('id')->on('scopes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scopes', function (Blueprint $table) {
            $table->dropForeign('scopes_scope_id_foreign');
        });

        Schema::dropIfExists('scopes');
    }
}
