<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bots', function (Blueprint $table) {
            $table->increments('id');
            $table->string('token', 128);
            $table->unsignedInteger('bot_api_id')->nullable()->default(null);
            $table->unsignedInteger('user_id');
            $table->boolean('is_bot')->nullable()->default(null);
            $table->string('first_name')->nullable()->default(null);
            $table->string('last_name')->nullable()->default(null);
            $table->string('username')->nullable()->default(null);
            $table->string('language_code', 8)->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('bots', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bots', function (Blueprint $table) {
            $table->dropForeign('bots_user_id_foreign');
        });

        Schema::dropIfExists('bots');
    }
}
