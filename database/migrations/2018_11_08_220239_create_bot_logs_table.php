<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBotLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bot_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bot_id');
            $table->boolean('is_bot')->nullable()->default(null);
            $table->string('first_name')->nullable()->default(null);
            $table->string('last_name')->nullable()->default(null);
            $table->string('username')->nullable()->default(null);
            $table->string('language_code', 8)->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('bot_logs', function (Blueprint $table) {
            $table->foreign('bot_id')->references('id')->on('bots');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bot_logs', function (Blueprint $table) {
            $table->dropForeign('bot_logs_bot_id_foreign');
        });


        Schema::dropIfExists('bot_logs');
    }
}
