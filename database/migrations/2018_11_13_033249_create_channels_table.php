<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('channels', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bot_id');
            $table->double('channel_api_id');

            $table->string('title');
            $table->string('description')->nullable()->default(null);
            $table->string('username');
            $table->string('photo')->nullable()->default(null);

            $table->string('invite_link')->nullable()->default(null);

            $table->timestamps();
        });

        Schema::table('channels', function (Blueprint $table) {
            $table->foreign('bot_id')->references('id')->on('bots');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('channels', function (Blueprint $table) {
            $table->dropForeign('channels_bot_id_foreign');
        });

        Schema::dropIfExists('channels');
    }
}
